var editor = ace.edit("editor");
editor.setTheme("ace/theme/monokai");
editor.setShowPrintMargin(false);
editor.session.setMode("ace/mode/javascript");

var dirty;
// Set dirty event
editor.on("input", function() {
  var isClean = editor.session.getUndoManager().isClean();
  if( !isClean ) {
    if ( !dirty ) {
      // Set data state to dirty
      window.parent.postMessage({
        'name': 'updateStatus',
        'data': 'dirty'
      }, '*');
    }
    dirty = true;
  } else {
    if ( dirty ) {
      // Set data state to clean.
      window.parent.postMessage({
        'name': 'updateStatus',
        'data': 'clean'
      }, '*');
    }
    dirty = false;
  }
});


// Handle OCCAM events
window.addEventListener('message', function(event) {
  if (event.data.name === 'updateConfiguration') {
    // TODO: Update editor configuration
  }
  else if (event.data.name === 'updateStatus') {
    if (event.data.data == "ready") {
      // Maximize the widget
      window.parent.postMessage({
        'name': 'updateSize',
        'data': {
          'height': "100%"
        }
      }, '*');

      // Request input data
      window.parent.postMessage({
        'name': 'updateInput'
      }, '*');
    }
  }
  else if (event.data.name === 'updateInput') {
    var objectInfo = event.data.data;

    inputInfo = objectInfo;

    // Get the filename to pull the data
    var filename = objectInfo.file;
    var url = "/"     + objectInfo.id       +
              "/"     + objectInfo.revision +
              "/raw/" + filename;

    var modelist = ace.require("ace/ext/modelist");
    var mode = modelist.getModeForPath(filename);
    // mode.name (small name), mode.caption (description name), mode.mode (module path)
    editor.session.setMode(mode.mode);

    // Pull the data
    fetch(url, {
      credentials: 'include'
    }).then(function(response) {
      return response.text();
    }).then(function(text) {
      editor.setValue(text, -1); // -1 moves the cursor to the start (without this,
                                 // it will select the entire text)
      editor.getSession().setUndoManager(new ace.UndoManager());
    });
  }
});

var saveButton = document.body.querySelector("button#save-button");
saveButton.addEventListener("click", function(event) {
  var data = editor.getValue();
  window.parent.postMessage({
    'name': 'updateFile',
    'file': inputInfo.file,
    'data': data
  }, '*');
  // TODO: Background update without refresh!!!
  //        Right now saving a file will refresh page, so this is useless!
  editor.session.getUndoManager().markClean();
  dirty = false;
  // Set data state to clean.
  window.parent.postMessage({
    'name': 'updateStatus',
    'data': 'clean'
  }, '*');

});

// We are ready.

// Request configuration data.
window.parent.postMessage({
  'name': 'updateConfiguration'
}, '*');

// Request input file (if any)
window.parent.postMessage({
  'name': 'updateInput'
}, '*');
